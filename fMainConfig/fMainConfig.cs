﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace fMainConfig {
    public partial class fMainConfig : Form {
        public fMainConfig() {
            InitializeComponent();

            Setup();
        }
        private void fMainConfig_Load(object sender, EventArgs e) {
            this.Close();
        }

        public SetupPay.FormPay setupPay = new SetupPay.FormPay();
        public Tester tester = new Tester();
        public DataLog dataLog = new DataLog();
        public TcpIP tcpIP = new TcpIP();
        public PrismTest prismTest = new PrismTest();
        public UpDataTest upDataTest = new UpDataTest();
        public ConfigUI configUI = new ConfigUI();
        public ConfigScript configScript = new ConfigScript();


        private void Setup() {
            setupPay.SelectTab = SetupPay.tabPage.TAB1;
            setupPay.set_nameTab(tester.nameFile);
            setupPay.SelectTab = SetupPay.tabPage.TAB2;
            setupPay.set_nameTab(prismTest.nameFile);
            setupPay.SelectTab = SetupPay.tabPage.TAB3;
            setupPay.set_nameTab(upDataTest.nameFile);
            setupPay.SelectTab = SetupPay.tabPage.TAB4;
            setupPay.set_nameTab(tcpIP.nameFile);
            setupPay.SelectTab = SetupPay.tabPage.TAB5;
            setupPay.set_nameTab(dataLog.nameFile);
            setupPay.SelectTab = SetupPay.tabPage.TAB6;
            setupPay.set_nameTab(configUI.nameFile);
            setupPay.SelectTab = SetupPay.tabPage.TAB7;
            setupPay.set_nameTab(configScript.nameFile);
            setupPay.setup();

            setupPay.form1.Icon = this.Icon;

            setupPay.show_dialog();
        }


        public class DataLog {
            /// <summary>nameFile = "datalog_config"</summary>
            public string nameFile { get; set; }

            public DataLog() {
                nameFile = "datalog_config";
            }
        }
        public class Tester {
            /// <summary>nameFile = "tester_config"</summary>
            public string nameFile { get; set; }

            public Tester() {
                nameFile = "tester_config";
            }
        }
        public class TcpIP {
            /// <summary>nameFile = "tcptp_config"</summary>
            public string nameFile { get; set; }

            public TcpIP() {
                nameFile = "tcptp_config";
            }
        }
        public class PrismTest {
            /// <summary>nameFile = "prism_config"</summary>
            public string nameFile { get; set; }

            public PrismTest() {
                nameFile = "prism_config";
            }
        }
        public class UpDataTest {
            /// <summary>nameFile = "up_data_config"</summary>
            public string nameFile { get; set; }

            public UpDataTest() {
                nameFile = "up_data_config";
            }
        }
        public class ConfigUI {
            public string nameFile { get; set; }

            public ConfigUI() {
                nameFile = "UI_config";
            }
        }
        public class ConfigScript {
            public string nameFile { get; set; }

            public ConfigScript() {
                nameFile = "Script_config";
            }
        }
    }
}
